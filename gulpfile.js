var gulp = require('gulp');
var gutil = require('gulp-util');
var compass = require('gulp-compass');

var sassSources = ['sass/**/*.scss'];

gulp.task('sass', function(){
	gulp.src(sassSources)
	.pipe(compass({
		sass: 'sass',
		css: '',
		image: 'images',
		style: 'expanded'	
	}))
	.on('error', function(err){ console.log(err.message)})
	.pipe(gulp.dest(''))

})

gulp.task('watch', function(){
	gulp.watch(sassSources, ['sass']);
});

gulp.task('default', ['sass', 'watch']);