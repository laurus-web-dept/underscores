To use this Repo:

1. Create a new folder
2. Git Bash Here in the new folder
3. $ git clone http://bitbucket.org/laurus-web-dept/underscores.git .
4. $ npm install
5. Copy everything from the new folder to your underscores theme folder
6. $ gulp

Now by editing any of the /sass files the gulpfile will automatically turn them into style.css